terraform {
  backend "s3" {
    bucket = "practice1-mothes3bucket"
    key    = "ec2-dev/terraform.tfstate"
    region = "ap-south-1"
  }
}