output "VPC_ID" {
    value = aws_vpc.main.id 
}

output "DEFAULT_VPC_ID" {
    value = var.DEFAULT_VPC_ID
}

output "VPC_CIDR" {
    value = var.VPC_CIDR
}

output "PRIVATE_SUBNETS" {
    value = aws_subnet.public.*.id
}

output "PUBLIC_SUBNETS" {
    value = aws_subnet.public.*.id
}
