resource "aws_instance" "web" {
    ami                       = data.aws_ami.ami.id
    vpc_security_group_ids    = [aws_security_group.student_sg.id]
    subnet_id                 = data.terraform_remote_state.vpc.outputs.PUBLIC_SUBNETS[0]
    instance_type             = "t2.micro"
    key_name                  = "mykey"


  tags = {
    Name = "${var.PROJECTNAME}-dev"
  }
}