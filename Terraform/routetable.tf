resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

route {
    cidr_block = data.aws_vpc.default.cidr_block
    vpc_peering_connection_id  = aws_vpc_peering_connection.peer.id
  }

   tags = {
     Name = "${var.PROJECTNAME}-public-rt"
  }
}

resource "aws_route_table_association" "public" {
    count      = length(var.PUB_SUBNET_CIDR)
    subnet_id = element(aws_subnet.public.*.id, count.index)
    route_table_id = aws_route_table.public.id
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id

route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.ngw.id
  }

 tags = {
     Name = "${var.PROJECTNAME}-private-rt"
  }
}


resource "aws_route_table_association" "private" {
    count      = length(var.PRV_SUBNET_CIDR)
    subnet_id = element(aws_subnet.private.*.id, count.index)
    route_table_id = aws_route_table.private.id
}

resource "aws_route" "data" {
  route_table_id            = data.aws_route_table.rt.id
  destination_cidr_block    = var.VPC_CIDR
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
}