data "aws_vpc" "default" {
  id = var.DEFAULT_VPC_ID
}

data "aws_route_table" "rt" {
  vpc_id = var.DEFAULT_VPC_ID
}