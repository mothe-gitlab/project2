resource "aws_vpc_peering_connection" "peer" {
  peer_owner_id = aws_vpc.main.owner_id
  peer_vpc_id   = aws_vpc.main.id
  vpc_id        = data.aws_vpc.default.id
  auto_accept   = true
}