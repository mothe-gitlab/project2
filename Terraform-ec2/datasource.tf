data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = var.BUCKET
    key    = "vpc-dev/terraform.tfstate"
    region = var.REGION
  }
}

data "aws_ami" "ami" {
  most_recent      = true
  owners           = ["876385101711"]

  filter {
    name   = "name"
    values = ["manuAMI"]
 }
}